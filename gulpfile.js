var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create();
var runSequence = require('run-sequence');
var rimraf = require('gulp-rimraf');

var DEST = 'public/';

gulp.task('clean', function() {
  return gulp.src([DEST], {
      read: false
    })
    .pipe(rimraf());
});

gulp.task('copy', function() {
    gulp.src('src/html/**')
        .pipe(gulp.dest(DEST))
    gulp.src('src/images/**/*')
        .pipe(gulp.dest(DEST + '/images'))
        gulp.src('src/css/**/*')
            .pipe(gulp.dest(DEST + '/css'))
});

gulp.task('scripts', function() {
    return gulp.src([
        'src/js/helpers/*.js',
        'src/js/*.js',
      ])
      .pipe(concat('custom.js'))
      .pipe(gulp.dest(DEST+'/js'))
      .pipe(rename({suffix: '.min'}))
      .pipe(uglify())
      .pipe(gulp.dest(DEST+'/js'))
      .pipe(browserSync.stream());
});

// TODO: Maybe we can simplify how sass compile the minify and unminify version
var compileSASS = function (filename, options) {
  return sass('src/scss/*.scss', options)
        .pipe(autoprefixer('last 2 versions', '> 5%'))
        .pipe(concat(filename))
        .pipe(gulp.dest(DEST+'/css'))
        .pipe(browserSync.stream());
};

gulp.task('sass', function() {
    return compileSASS('custom.css', {});
});

gulp.task('sass-minify', function() {
    return compileSASS('custom.min.css', {style: 'compressed'});
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: './public'
        },
        startPath: './index.html'
    });
});

gulp.task('watch', function() {
  // Watch .html files
  gulp.watch('src/html/*.html', ['copy', browserSync.reload]);
  // Watch .js files
  gulp.watch('src/js/*.js', ['copy', 'scripts']);
  // Watch .scss files
  gulp.watch('src/scss/*.scss', ['sass', 'sass-minify']);
});

gulp.task('build', function(callback) {
  runSequence(
      'clean',
      'copy',
      'scripts');
});
// Default Task
gulp.task('default', ['copy', 'browser-sync', 'watch']);
